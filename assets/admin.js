// https://codebeautify.org/minify-js
class AcfBlockTitle extends HTMLElement {
    constructor() {
        super();

        this.isInited = false
    }

    connectedCallback() {
        'loading' === document.readyState ?
            document.addEventListener('DOMContentLoaded', this.setup.bind(this))
            : this.setup()
    }

    getSibling(selector) {
        return this.closest('.acf-block-body').querySelector('.acf-block-fields > ' + selector)
    }

    getKeyField() {
        let keyField = ''
        let mainInput = this.getSibling('.acf-block-main-field')

        mainInput = mainInput ?
            mainInput :
            this.getSibling('.acf-field-text')

        if (!mainInput) {
            return ''
        }

        keyField = mainInput.querySelector('input').value
        if (keyField.length > 50) {
            keyField = keyField.substring(0, 50) + ' ...'
        }

        return '"' + keyField + '"'
    }

    activateDefaultTab() {
        let defaultTabInput = this.getSibling('.acf-block-default-tab')
        if (!defaultTabInput) {
            return
        }

        let dataKey = defaultTabInput.dataset['key'] || ''
        let tabWrapper = this.getSibling('.acf-tab-wrap')

        if (!tabWrapper) {
            return
        }

        let defaultTab = tabWrapper.querySelector('.acf-tab-button[data-key="' + dataKey + '"]')
        defaultTab.dispatchEvent(new Event('click', {
            bubbles: true,
        }))
    }

    setup() {
        let parentBlock = this.closest('.wp-block')
        // skip for non blocks
        if (!parentBlock) {
            return
        }

        // NavigationThemeClassic
        let title = this.closest('.wp-block').getAttribute('data-title') || ''
        title = title.split('Theme')

        let blockTitle = title[0] || ''

        this.innerHTML = '<h3>' + this.getKeyField() + '</h3><h3>' + blockTitle + '</h3>'

        this.addEventListener('click', this.toggle.bind(this))

        this.parentElement.classList.add('acf-block-fields--inited')
    }

    toggle() {
        if (!this.isInited) {
            this.isInited = true
            this.activateDefaultTab()
        }

        this.parentElement.classList.toggle('acf-block-fields--active')
    }
}

customElements.define('acf-block-title', AcfBlockTitle)