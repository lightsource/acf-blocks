# Acf blocks

## What is it?

Extension of the [front-blocks package](https://gitlab.com/lightsource/front-blocks) for WordPress, takes care of about
enquiring CSS/JS, adds ajax supporting for blocks, allows to create Gutenberg blocks super easily (with just one
constant, under the hood based on [ACF blocks feature](https://wplake.org/blog/acf-blocks/))

## How to use

1. Install the package  
   `composer require lightsource/acf-blocks`
2. Require the composer autoloader  
   `require_once __DIR__ . '/vendors/vendor/autoload.php';`
3. Use the `LightSource\AcfBlocks\AcfBlock` class as blocks' parent instead of `LightSource\FrontBlocks\Block`, and
   override constant values to `true` (when a feature is necessary). Callback methods, `ajax()`
   and `loadByAcf()` are optional and missing in the class (so you will declare them, not override), to
   provide an
   opportunity of dynamic argument injection for them (see more in the Advanced section).

```injectablephp
use LightSource\AcfBlocks\AcfBlock;

class FirstBlock extends AcfBlock
{
    // acf block will be signed up,
    // css/js code of the block will be enqueued on admin pages with the Gutenberg editor
    // during rendering 'loadByAcf()' method will be called
    const IS_SUPPORT_GUTENBERG = true;
    
    // css/js code of the block will be enqueued on all admin pages
    const IS_SUPPORT_DASHBOARD = true;
    
    // wp ajax listener will be added for the block, static 'ajax()' method will be called,
    // in js code an 'acf_blocks' variable with an ajax object will be available
    // (acf_blocks.[blockClassNameWithoutNamespace].ajax.URL and acf_blocks.[blockClassNameWithoutNamespace].ajax.NAME)
    const IS_SUPPORT_AJAX = true;
}
```

4. Add this line `<!--acf_blocks_css-->` to a head tag of front-end pages, it'll be replaced with blocks' css (it'll
   give better speed than enqueue like a file).  
   For admin pages with Gutenberg editor (page/post) css of all blocks that support Gutenberg will be added via one
   separate file.
5. Create an `AcfBlocks` instance

```injectablephp
use LightSource\AcfBlocks\Settings;
use LightSource\AcfBlocks\AcfBlocks;

// set up the FrontBlocks package, below $frontBlocks is an instance of the FrontBlocks class 

// create a folder within a theme or plugin where .css/.js files for pages will be stored
$settings = new Settings('absPathToAssetsFolder', 'urlOfAssetsFolder');
$acfBlock = new AcfBlocks(
    $settings,
    $frontBlocks->getLoader(),
    $frontBlocks->getCreator(),
    $frontBlocks->getRenderer(),
    $frontBlocks->getResourceCreator()
);
$acfBlock->setup();
```

6. That's it, enjoy.

## Advanced

### 1. WordPress resources

**Front-end**

3 different types  : post (with page, with CPTs because it's also a post inside), taxonomy, search.
CSS and JS of used blocks will be saved into files with id, e.g. `post-1.min.css`, `post-3.min.js`. In this way every
page could have own set of blocks (within the Gutenberg editor or even within a pure WP page template). JS file will
be enqueued as a file, CSS code will be inserted into a page source (to speed up).

**Admin dashboard**

3 different types : page, post, common (admin). The first couple will be enqueued only on
related pages with the Gutenberg editor, the last will be enqueued on all dashboard pages.

**Extend**

You can override static `getJsData()` method to pass more arguments into js code.  
You can override static `getExternalJsDependencies()` method to declare external WP dependencies of js code,
like `jquery`

### 2. ACF block

The package supports dynamic injection for the `ajax()` and `loadByAcf()` methods. It means you can use any
class-typed arguments in these methods, and they'll be requested from the container (you need to set the container in
the `front blocks` package). So you can declare `ajax(DependencyClass $dependency, SecondClass $secondDependency)`
or `loadByAcf(DependencyClass $dependency)` and it'll work properly.

If you need to get the current `postId` in the `loadByAcf()`
method, then declare an argument with the `AcfData` type, or any class that implements `AcfDataInterface`. You can even
define the argument type as the interface, but don't forget then to
define it in the container, like `$container->set(AcfDataInterface::class, YourClass::class)`

Also, you can override static `getGutenbergArguments()` method to change ACF block attributes.

### 3. Classes field

`AcfBlock` class (that your blocks extend) contains a `classes` array field, and a block class name will be added
automatically (in a constructor), so you might use the field in twig. (CamelCase will be converted into camel-case,
a `Theme` word will be replaced with `--theme--`, so FirstBlockThemeRedOne will be first-block--theme--red-one)

### 4. Filters

`LightSource_AcfBlocks_UsedCss` and `LightSource_AcfBlocks_UsedJs` could be used for modifying resources before
saving, e.g. for adding extra CSS dynamically

### 5. Admin interface

Blocks inside the Gutenberg editor are wrapped into accordions. Accordion title reflects content of the main field along
with the type of the block.
By default, the main field is the first text field. Use '@a-main 1' attribute to mark another as main.  
By default, the first tab is an active. Use '@a-default 1' attribute to mark another as default.

### 6. Benchmark

On a real website. Page with 45 unique blocks on the page and 164 blocks in the library:

Common time: 0.101 seconds  
Load time: 0.031 seconds per 164 blocks  
Creation time: 0.00619 seconds per 12 blocks (only 12 main blocks, others are inner)  
Render time: 0.057 seconds per 45 renders  
Resource combine time: 0.00684 seconds per 2 combines

Hardware: Hetzner VPS, 4vCPU, 8GB RAM, 160GB SSD  
PHP 8.1 with opcache enabled