<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

use LightSource\AcfBlocks\Interfaces\AcfBlockInterface;

use LightSource\FrontBlocks\Block\Block;

abstract class AcfBlock extends Block implements AcfBlockInterface
{
    // should be overridden to enable features
    const IS_SUPPORT_GUTENBERG = false;
    const IS_SUPPORT_DASHBOARD = false;
    const IS_SUPPORT_AJAX = false;
    // can be overridden
    const GUTENBERG_CATEGORY = 'AcfBlocks';

    protected array $classes;

    public function __construct()
    {
        parent::__construct();

        $this->addBlockClass();
    }

    protected function addBlockClass(string $className = ''): void
    {
        // SomeBlockThemeMain to some-block--theme--main
        $fullClassName = $className ?: get_class($this);
        $nameParts     = explode('\\', $fullClassName);
        $className     = $nameParts[count($nameParts) - 1];

        $className = lcfirst($className);
        $nameParts = preg_split('/(?=[A-Z])/', $className);
        foreach ($nameParts as &$namePart) {
            $namePart = lcfirst($namePart);
        }
        $className = implode('-', $nameParts);
        $className = str_replace('-theme-', '--theme--', $className);

        $this->classes[] = $className;
    }

    public static function getJsData(): array
    {
        $jsData = [];

        if (static::IS_SUPPORT_AJAX &&
            function_exists('get_admin_url')) {
            $jsData['ajax'] = [
                'URL'  => get_admin_url(null, 'admin-ajax.php'),
                'NAME' => static::getName(),
            ];
        }

        return $jsData;
    }

    public static function getExternalJsDependencies(): array
    {
        return [];
    }

    public static function getName(): string
    {
        // 'Namespace\Blocks\FirstBlock\FirstBlock' to 'namespace_blocks_firstblock_firstblock'

        $fullClassName = static::class;
        $nameParts     = explode('\\', $fullClassName);

        $name = implode('_', $nameParts);
        $name = strtolower($name);

        return $name;
    }

    public static function getAcfName(): string
    {
        // acf_register_block_type() method always replaces underlines to dash
        // so have to prepare here, to have the right name for other places
        return str_replace('_', '-', static::getName());
    }

    public static function getAjaxName(): string
    {
        return static::getName();
    }

    public static function getGutenbergArguments(): array
    {
        // 'FirstBlock' from 'Namespace\Blocks\FirstBlock\FirstBlock'
        $title = explode('\\', static::class);
        $title = $title[count($title) - 1];

        return [
            'title'       => $title,
            'description' => '',
            'category'    => static::GUTENBERG_CATEGORY,
        ];
    }

    public static function isSupportGutenberg(): bool
    {
        return static::IS_SUPPORT_GUTENBERG;
    }

    public static function isSupportDashboard(): bool
    {
        return static::IS_SUPPORT_DASHBOARD;
    }

    public static function isSupportAjax(): bool
    {
        return static::IS_SUPPORT_AJAX;
    }
}
