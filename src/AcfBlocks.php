<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

use LightSource\AcfBlocks\Interfaces\{
    AcfBlocksInterface,
    SettingsInterface
};
use LightSource\FrontBlocks\Block\ResourceCreator;
use LightSource\FrontBlocks\Interfaces\{
    CreatorInterface,
    LoaderInterface,
    RendererInterface
};

class AcfBlocks implements AcfBlocksInterface
{
    protected LoaderInterface $loader;
    protected ResourcesManager $resourcesManager;
    protected AdminAssets $adminAssets;
    protected BlocksSetuper $blocksSetuper;
    protected SettingsInterface $settings;

    public function __construct(
        SettingsInterface $settings,
        LoaderInterface $loader,
        CreatorInterface $creator,
        RendererInterface $renderer,
        ResourceCreator $resourceCreator
    ) {
        $this->settings = $settings;
        $this->loader = $loader;
        $this->resourcesManager = new ResourcesManager($settings, $loader, $renderer);
        $this->adminAssets = new AdminAssets();

        $this->blocksSetuper = new BlocksSetuper($creator, $renderer, $settings, $resourceCreator);

        $this->loader->setBlocksSetuper($this->blocksSetuper);
        $renderer->setRenderContent($this->resourcesManager);
    }

    public function setup(string $phpFilePreg = '/.php$/'): void
    {
        $this->loader->loadAllBlocks($phpFilePreg);
        $this->resourcesManager->setup();
        $this->adminAssets->setup();
    }

    public function getBlocksSetuper(): BlocksSetuper
    {
        return $this->blocksSetuper;
    }

    public function getSettings(): SettingsInterface
    {
        return $this->settings;
    }
}
