<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

use LightSource\AcfBlocks\Interfaces\AcfDataInterface;

class AcfData implements AcfDataInterface
{
    private int $postId;

    public function __construct()
    {
        $this->postId = 0;
    }

    public function getPostId(): int
    {
        return $this->postId;
    }

    public function setPostId(int $postId): void
    {
        $this->postId = $postId;
    }
}
