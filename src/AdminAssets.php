<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

class AdminAssets
{
    public function addCustomClassesToFieldWrapper(array $wrapper, array $field): array
    {
        if (!key_exists('a-main', $field) &&
            !key_exists('a-default', $field)) {
            return $wrapper;
        }

        if (!key_exists('class', $wrapper)) {
            $wrapper['class'] = '';
        }

        if (key_exists('a-main', $field)) {
            $wrapper['class'] .= ' acf-block-main-field';
        }

        if (key_exists('a-default', $field)) {
            $wrapper['class'] .= ' acf-block-default-tab';
        }

        return $wrapper;
    }

    public function printAcfBlockTitle($fields, $postId): void
    {
        if (!function_exists('acf_is_block_editor') ||
            !acf_is_block_editor()) {
            return;
        }

        echo '<acf-block-title class="acf-block-title"></acf-block-title>';
    }

    public function printAdminJs(): void
    {
        if (!function_exists('acf_is_block_editor') ||
            !acf_is_block_editor()) {
            return;
        }

        echo '<script class="acf-blocks-js">' .
            file_get_contents(__DIR__ . '/../assets/admin.min.js') .
            '</script>';
    }

    public function printAdminCss(): void
    {
        if (!function_exists('acf_is_block_editor') ||
            !acf_is_block_editor()) {
            return;
        }

        echo '<style class="acf-blocks-css">' .
            file_get_contents(__DIR__ . '/../assets/admin.min.css') .
            '</style>';
    }

    public function setup(): void
    {
        if (!function_exists('add_action') ||
            !function_exists('add_filter')) {
            return;
        }

        add_action('acf/render_fields', [$this, 'printAcfBlockTitle'], 10, 2);
        add_action('admin_print_scripts', [$this, 'printAdminJs']);
        add_action('admin_head', [$this, 'printAdminCss']);

        add_filter('acf/field_wrapper_attributes', [$this, 'addCustomClassesToFieldWrapper'], 10, 2);
    }
}
