<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

use Exception;
use LightSource\AcfBlocks\Interfaces\AcfBlockInterface;
use LightSource\AcfBlocks\Interfaces\AcfDataInterface;
use LightSource\AcfBlocks\Interfaces\SettingsInterface;
use LightSource\FrontBlocks\Block\ResourceCreator;
use LightSource\FrontBlocks\Interfaces\BlocksSetuperInterface;
use LightSource\FrontBlocks\Interfaces\CreatorInterface;
use LightSource\FrontBlocks\Interfaces\RendererInterface;
use Psr\Container\ContainerInterface;
use ReflectionClass;

class BlocksSetuper implements BlocksSetuperInterface
{
    protected CreatorInterface $creator;
    protected RendererInterface $renderer;
    protected SettingsInterface $settings;
    protected ResourceCreator $resourceCreator;
    protected float $creationTimeInSeconds;
    protected int $creationNumber;

    public function __construct(
        CreatorInterface $creator,
        RendererInterface $renderer,
        SettingsInterface $settings,
        ResourceCreator $resourceCreator
    ) {
        $this->creator = $creator;
        $this->renderer = $renderer;
        $this->settings = $settings;
        $this->resourceCreator = $resourceCreator;
        $this->creationTimeInSeconds = 0;
        $this->creationNumber = 0;
    }

    protected function renderPreview(string $blockClass): void
    {
        if (!$this->settings->getUrlOfBlocksFolder()) {
            return;
        }

        $resource = $this->resourceCreator->createResource($blockClass);
        $relativePreviewFilePath = $resource->getRelativePath() . $this->settings->getPreviewExtension();
        $absPreviewFilePath = $resource->getFolder() . DIRECTORY_SEPARATOR . $relativePreviewFilePath;
        $urlOfPreviewFile = $this->settings->getUrlOfBlocksFolder() . '/' . $relativePreviewFilePath;

        if (file_exists($absPreviewFilePath)) {
            echo "<img src='{$urlOfPreviewFile}' alt='preview' style='width: 100%;' />";
        }
    }

    protected function getClasses(array $blockData, bool $isPreview): array
    {
        $classes = [];

        if (isset($blockData['className']) &&
            $blockData['className']) {
            $classNames = explode(' ', $blockData['className']);
            $classNames = array_filter($classNames, function ($className) {
                return false === strpos($className, 'wp-block-acf-blocks-');
            });
            $classes = array_merge($classes, $classNames);
        }

        if ($isPreview) {
            $classes[] = 'acf-blocks--preview';
        }

        return $classes;
    }

    /**
     * @throws Exception
     */
    protected function getMethodArgumentTypes($objectOrClass, string $methodName): ?array
    {
        $methodArguments = [];

        $className = is_object($objectOrClass) ?
            get_class($objectOrClass) :
            $objectOrClass;

        try {
            $classInfo = new ReflectionClass($className);
            $methodInfo = $classInfo->getMethod($methodName);
        } catch (Exception $exception) {
            throw new Exception(
                'Fail to get method arguments, class : ' .
                $className . ', method : ' . $methodName
            );
        }

        $parameters = $methodInfo->getParameters();
        foreach ($parameters as $parameter) {
            $parameterClass = $parameter->getType();
            $parameterClass = $parameterClass ?
                $parameterClass->getName() :
                '';
            $parameterClass = $parameterClass &&
            (class_exists($parameterClass) || interface_exists($parameterClass)) ?
                $parameterClass :
                '';

            if (!$parameterClass) {
                throw new Exception(
                    'Fail to get method arguments, class : ' .
                    $className . ', method : ' . $methodName
                );
            }

            $methodArguments[] = $parameterClass;
        }

        return $methodArguments;
    }

    protected function render(
        string $blockClass,
        array $blockData,
        string $content,
        bool $isPreview,
        int $postId,
        ?ContainerInterface $container
    ): void {
        if (!function_exists('get_field')) {
            return;
        }

        if (get_field('__mini-preview')) {
            $this->renderPreview($blockClass);

            return;
        }

        $creationStartTime = microtime(true);

        // unique instance, one page can have multiple blocks with the same type
        $acfBlock = $this->creator->create($blockClass);

        $creationEndTime = microtime(true);
        $this->creationTimeInSeconds += $creationEndTime - $creationStartTime;
        $this->creationNumber++;

        $this->callMethodWithDynamicArguments($acfBlock, 'loadByAcf', $container, [
            'postId' => $postId,
        ]);

        $this->renderer->render($acfBlock, [
            '_isGutenberg' => true,
            'classes' => $this->getClasses($blockData, $isPreview),
        ], true);
    }

    protected function signUpInACF(
        string $blockClass,
        ?ContainerInterface $container
    ): void {
        if (!function_exists('acf_register_block_type')) {
            return;
        }

        $acfName = call_user_func([$blockClass, 'getAcfName']);
        $gutenbergArguments = call_user_func([$blockClass, 'getGutenbergArguments']);

        $acfDefaultArgs = [
            'name' => $acfName,
            'mode' => 'edit',
            'supports' => [
                // don't use, it causes issues at the gutenberg when WebComponents have used
                // 'jsx' => true,
                'mode' => false,
            ],
            'render_callback' => function ($blockData, $content = '', $isPreview = false, $postId = 0) use (
                $blockClass,
                $container
            ) {
                $blockData = is_array($blockData) ?
                    $blockData :
                    [];
                $content = (string)$content;
                $isPreview = (bool)$isPreview;
                // for some reasons sometimes it's a string
                $postId = (int)$postId;

                $this->render(
                    $blockClass,
                    $blockData,
                    $content,
                    $isPreview,
                    $postId,
                    $container
                );
            },
        ];

        $acfArgs = array_replace_recursive($acfDefaultArgs, $gutenbergArguments);

        acf_register_block_type($acfArgs);
    }

    public function setupBlock(
        string $blockClass,
        ?ContainerInterface $container
    ): void {
        if (!function_exists('add_action') ||
            !function_exists('is_admin') ||
            !in_array(AcfBlockInterface::class, class_implements($blockClass), true)) {
            return;
        }

        $isSupportAjax = call_user_func([$blockClass, 'isSupportAjax']);
        $isSupportDashboard = call_user_func([$blockClass, 'isSupportDashboard']);
        $isSupportGutenberg = call_user_func([$blockClass, 'isSupportGutenberg']);

        if ($isSupportAjax) {
            $ajaxName = call_user_func([$blockClass, 'getAjaxName']);
            $ajaxCallback = function () use ($blockClass, $container) {
                $this->callMethodWithDynamicArguments($blockClass, 'ajax', $container);
            };
            add_action("wp_ajax_" . $ajaxName, $ajaxCallback);
            add_action("wp_ajax_nopriv_" . $ajaxName, $ajaxCallback);
        }

        if ($isSupportDashboard &&
            is_admin()) {
            // todo improve
            $acfBlock = $this->creator->create($blockClass);
            // render adds the block to the used list
            $this->renderer->render($acfBlock);
        }

        if ($isSupportGutenberg) {
            // acf_register_block_type() method should be called in the acf init action
            add_action('acf/init', function () use ($blockClass, $container) {
                $this->signUpInACF($blockClass, $container);
            });
        }
    }

    /**
     * @throws Exception
     */
    public function callMethodWithDynamicArguments(
        $objectOrClass,
        string $methodName,
        ?ContainerInterface $container,
        array $defaultArguments = []
    ): void {
        // method is optional
        if (!method_exists($objectOrClass, $methodName)) {
            return;
        }

        $argumentTypes = $this->getMethodArgumentTypes($objectOrClass, $methodName);

        $arguments = [];

        if ($container) {
            foreach ($argumentTypes as $argumentType) {
                $argument = $container->get($argumentType);

                if (is_a($argument, AcfDataInterface::class)) {
                    $argument->setPostId($defaultArguments['postId'] ?? 0);
                }

                $arguments[] = $argument;
            }
        }

        call_user_func_array([$objectOrClass, $methodName], $arguments);
    }

    public function getCreationTimeInSeconds(): float
    {
        return $this->creationTimeInSeconds;
    }

    public function getCreationNumber(): int
    {
        return $this->creationNumber;
    }
}
