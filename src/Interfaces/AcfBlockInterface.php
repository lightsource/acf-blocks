<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks\Interfaces;

use LightSource\FrontBlocks\Interfaces\BlockInterface;

interface AcfBlockInterface extends BlockInterface
{
    public static function isSupportGutenberg(): bool;

    public static function isSupportDashboard(): bool;

    public static function isSupportAjax(): bool;

    public static function getJsData(): array;

    public static function getExternalJsDependencies();

    public static function getName();

    public static function getAjaxName();

    public static function getAcfName();

    public static function getGutenbergArguments();
}
