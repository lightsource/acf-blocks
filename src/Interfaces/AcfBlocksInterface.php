<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks\Interfaces;

interface AcfBlocksInterface
{
    public function setup(string $phpFilePreg = '/.php$/'): void;

    public function getSettings(): SettingsInterface;
}
