<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks\Interfaces;

interface AcfDataInterface
{
    public function getPostId(): int;

    public function setPostId(int $postId): void;
}
