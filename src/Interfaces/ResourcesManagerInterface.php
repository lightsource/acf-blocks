<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks\Interfaces;

interface ResourcesManagerInterface
{
    public function saveFrontResources(): void;

    public function enqueueFrontResources(): void;

    public function saveAdminResources(): void;

    public function enqueueAdminResources(): void;

    public function setup(): void;
}
