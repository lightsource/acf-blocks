<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks\Interfaces;

interface SettingsInterface
{
    public function setPathToAssetsFolder(string $pathToAssetsFolder): void;

    public function setUrlOfAssetsFolder(string $urlOfAssetsFolder): void;

    public function setUrlOfBlocksFolder(string $urlOfBlocksFolder): void;

    public function setIsIncludeSource(bool $isIncludeSource): void;

    public function setAssetsVersion(string $assetsVersion): void;

    public function setResourceName(string $resourceName): void;

    public function setJsExtension(string $jsExtension): void;

    public function setCssExtension(string $cssExtension): void;

    public function setPreviewExtension(string $previewExtension): void;

    public function setCustomFrontFileName(string $customFrontFileName): void;

    public function getPathToAssetsFolder(): string;

    public function getUrlOfAssetsFolder(): string;

    public function getUrlOfBlocksFolder(): string;

    public function isIncludeSource(): bool;

    public function getAssetsVersion(): string;

    public function getResourceName(): string;

    public function getJsExtension(): string;

    public function getCssExtension(): string;

    public function getPreviewExtension(): string;

    public function getCustomFrontFileName(): string;

    public function getLateHeadFilter(): string;

    public function setLateHeadFilter(string $lateHeadFilter): void;
}
