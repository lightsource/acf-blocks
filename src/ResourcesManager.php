<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

use Exception;
use LightSource\AcfBlocks\Interfaces\AcfBlockInterface;
use LightSource\AcfBlocks\Interfaces\ResourcesManagerInterface;
use LightSource\AcfBlocks\Interfaces\SettingsInterface;
use LightSource\FrontBlocks\Interfaces\BlockInterface;
use LightSource\FrontBlocks\Interfaces\LoaderInterface;
use LightSource\FrontBlocks\Interfaces\RenderContentInterface;
use LightSource\FrontBlocks\Interfaces\RendererInterface;

class ResourcesManager implements ResourcesManagerInterface, RenderContentInterface
{
    protected SettingsInterface $settings;
    protected LoaderInterface $loader;
    protected RendererInterface $renderer;
    protected array $externalJsDependencies;
    protected array $jsData;

    public function __construct(
        SettingsInterface $settings,
        LoaderInterface $loader,
        RendererInterface $renderer
    ) {
        $this->settings = $settings;
        $this->loader = $loader;
        $this->renderer = $renderer;
        $this->externalJsDependencies = [];
        $this->jsData = [];
    }

    protected function saveUsedResourcesIntoFile(string $fileNameWithoutExtension, int $queriedId): void
    {
        $js = $this->renderer->getUsedResources(
            $this->settings->getJsExtension(),
            $this->settings->isIncludeSource()
        );
        $css = $this->renderer->getUsedResources(
            $this->settings->getCssExtension(),
            $this->settings->isIncludeSource()
        );

        if (function_exists('apply_filters')) {
            $js = apply_filters('LightSource_AcfBlocks_UsedJs', $js, $queriedId);
            $css = apply_filters('LightSource_AcfBlocks_UsedCss', $css, $queriedId);
        }

        if ($js) {
            $pathToJsFile = $this->settings->getPathToAssetsFolder(
                ) . '/' . $fileNameWithoutExtension . $this->settings->getJsExtension();
            file_put_contents($pathToJsFile, $js);
        }

        if ($css) {
            $pathToCssFile = $this->settings->getPathToAssetsFolder(
                ) . '/' . $fileNameWithoutExtension . $this->settings->getCssExtension();
            file_put_contents($pathToCssFile, $css);
        }
    }

    protected function enqueueJsResourcesFile(string $fileNameWithoutExtension): void
    {
        $pathToJsFile = $this->settings->getPathToAssetsFolder(
            ) . '/' . $fileNameWithoutExtension . $this->settings->getJsExtension();
        $urlOfJsFile = $this->settings->getUrlOfAssetsFolder(
            ) . '/' . $fileNameWithoutExtension . $this->settings->getJsExtension();

        if (!function_exists('wp_enqueue_script') ||
            !function_exists('wp_enqueue_script') ||
            !function_exists('wp_localize_script')
        ) {
            return;
        }

        if (!is_file($pathToJsFile)) {
            return;
        }

        // e.g. comment-reply
        foreach ($this->externalJsDependencies as $externalDependency) {
            wp_enqueue_script($externalDependency);
        }

        wp_enqueue_script(
            $this->settings->getResourceName(),
            $urlOfJsFile,
            $this->externalJsDependencies,
            $this->settings->getAssetsVersion(),
            true
        );
        wp_localize_script($this->settings->getResourceName(), $this->settings->getResourceName(), $this->jsData);
    }

    protected function getCssTag(string $fileNameWithoutExtension): string
    {
        $pathToCssFile = $this->settings->getPathToAssetsFolder(
            ) . '/' . $fileNameWithoutExtension . $this->settings->getCssExtension();

        if (!is_file($pathToCssFile)) {
            return '';
        }

        $id = $this->settings->getResourceName();

        return '<style id="' . $id . '">' . file_get_contents($pathToCssFile) . '</style>';
    }

    protected function enqueueCssResourcesFile(string $fileNameWithoutExtension, string $pageContent = ''): string
    {
        $pathToCssFile = $this->settings->getPathToAssetsFolder(
            ) . '/' . $fileNameWithoutExtension . $this->settings->getCssExtension();
        $urlOfCssFile = $this->settings->getUrlOfAssetsFolder(
            ) . '/' . $fileNameWithoutExtension . $this->settings->getCssExtension();

        if (!is_file($pathToCssFile)) {
            return $pageContent;
        }

        // for admin part
        if (!$pageContent) {
            if (function_exists('wp_enqueue_style')) {
                wp_enqueue_style('acf_blocks', $urlOfCssFile, [], $this->settings->getAssetsVersion());
            }

            return '';
        }

        // inject to the header, otherwise styles in the footer will slowly the render time (a user will see it)
        // inject pure css, not a css file, it's for WebVitals

        $pageContent = str_replace(
            '<!--acf_blocks_css-->',
            $this->getCssTag($fileNameWithoutExtension),
            $pageContent
        );

        return $pageContent;
    }

    protected function getFrontFileNameWithoutExtension(): string
    {
        // for some cases custom name is required,
        // e.g. one woocommerce checkout page can have 2 different looks (checkout + thank you)
        $customFrontFileName = $this->settings->getCustomFrontFileName();
        if ($this->settings->getCustomFrontFileName()) {
            return $customFrontFileName;
        }

        if (!function_exists('get_queried_object_id') ||
            !function_exists('get_queried_object') ||
            !function_exists('get_queried_object') ||
            !function_exists('is_search')) {
            return '';
        }

        $queriedId = (int)get_queried_object_id();
        $isTerm = !!(get_queried_object()->term_id ?? 0);
        $isSearch = is_search();
        $type = 'post';
        $type = $isTerm ?
            'term' :
            $type;
        $type = $isSearch ?
            'search' :
            $type;

        // avoid possible conflicts between types (if an id is identical)
        return $type . "-" . $queriedId;
    }

    protected function getAdminFileNameWithoutExtension(): string
    {
        if (!function_exists('get_current_screen')) {
            return '';
        }

        $currentScreen = get_current_screen();
        $isGutenbergPage = in_array($currentScreen->id, ['page', 'post',], true) &&
            in_array($currentScreen->post_type, ['page', 'post',], true);

        return $isGutenbergPage ?
            $currentScreen->post_type :
            'admin';
    }

    protected function addJsDataRecursively(AcfBlockInterface $block): void
    {
        $dependencies = $block->getDependencies();

        foreach ($dependencies as $dependency) {
            if (!$dependency instanceof AcfBlockInterface) {
                continue;
            }

            $this->addJsDataRecursively($dependency);
        }

        $externalJsDependencies = $block::getExternalJsDependencies();
        $jsData = $block::getJsData();

        if ($jsData) {
            $shortClassName = explode('\\', get_class($block));
            $shortClassName = $shortClassName[count($shortClassName) - 1];
            $this->jsData[$shortClassName] = $jsData;
        }

        if ($externalJsDependencies) {
            $this->externalJsDependencies = array_merge($this->externalJsDependencies, $externalJsDependencies);
            $this->externalJsDependencies = array_unique($this->externalJsDependencies);
            $this->externalJsDependencies = array_values($this->externalJsDependencies);
        }
    }

    public function startBuffering(): void
    {
        ob_start();
    }

    public function getRenderContent(BlockInterface $block, string $content): string
    {
        if (!$block instanceof AcfBlockInterface) {
            return $content;
        }

        $this->addJsDataRecursively($block);

        return $content;
    }

    public function saveFrontResources(): void
    {
        if (!function_exists('get_queried_object_id')) {
            return;
        }

        $queriedId = (int)get_queried_object_id();

        $this->saveUsedResourcesIntoFile($this->getFrontFileNameWithoutExtension(), $queriedId);
    }

    public function enqueueFrontResources(): void
    {
        $fileNameWithoutExtension = $this->getFrontFileNameWithoutExtension();
        $this->enqueueJsResourcesFile($fileNameWithoutExtension);

        if ($this->settings->getLateHeadFilter()) {
            return;
        }

        echo $this->enqueueCssResourcesFile($fileNameWithoutExtension, ob_get_clean());
    }

    public function saveAdminResources(): void
    {
        if (!function_exists('get_current_screen')) {
            return;
        }

        $this->saveUsedResourcesIntoFile($this->getAdminFileNameWithoutExtension(), 0);
    }

    public function enqueueAdminResources(): void
    {
        $fileNameWithoutExtension = $this->getAdminFileNameWithoutExtension();

        $this->enqueueJsResourcesFile($fileNameWithoutExtension);
        $this->enqueueCssResourcesFile($fileNameWithoutExtension);
    }

    public function setup(): void
    {
        if (!function_exists('add_action') ||
            !function_exists('add_filter')) {
            return;
        }

        // front-end hooks
        if (!$this->settings->getLateHeadFilter()) {
            add_action('get_header', [$this, 'startBuffering']);
            add_action('wp_footer', function () {
                $this->saveFrontResources();
            },9);
        } else {
            add_filter($this->settings->getLateHeadFilter(), function ($html) {
                $this->saveFrontResources();
                return $html . $this->getCssTag($this->getFrontFileNameWithoutExtension());
            });
        }

        add_action('wp_footer', function () {
            $this->enqueueFrontResources();
        });

        // back-end hooks
        add_action('admin_enqueue_scripts', function () {
            $this->saveAdminResources();
            $this->enqueueAdminResources();
        });
    }
}
