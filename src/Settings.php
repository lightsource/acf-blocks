<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

use LightSource\AcfBlocks\Interfaces\SettingsInterface;

class Settings implements SettingsInterface
{
    protected string $pathToAssetsFolder;
    protected string $urlOfAssetsFolder;
    protected string $urlOfBlocksFolder;
    protected bool $isIncludeSource;
    protected string $assetsVersion;
    protected string $resourceName;
    protected string $jsExtension;
    protected string $cssExtension;
    protected string $previewExtension;
    protected string $customFrontFileName;
    protected string $lateHeadFilter;

    public function __construct(
        string $pathToAssetsFolder = '',
        string $urlOfAssetsFolder = ''
    ) {
        $this->pathToAssetsFolder = $pathToAssetsFolder;
        $this->urlOfAssetsFolder = $urlOfAssetsFolder;
        $this->urlOfBlocksFolder = '';
        $this->isIncludeSource = false;
        $this->assetsVersion = '';
        $this->resourceName = 'acf_blocks';
        $this->jsExtension = '.min.js';
        $this->cssExtension = '.min.css';
        $this->previewExtension = '.png';
        $this->customFrontFileName = '';
        $this->lateHeadFilter = '';
    }

    public function setPathToAssetsFolder(string $pathToAssetsFolder): void
    {
        $this->pathToAssetsFolder = $pathToAssetsFolder;
    }

    public function setUrlOfAssetsFolder(string $urlOfAssetsFolder): void
    {
        $this->urlOfAssetsFolder = $urlOfAssetsFolder;
    }

    public function setUrlOfBlocksFolder(string $urlOfBlocksFolder): void
    {
        $this->urlOfBlocksFolder = $urlOfBlocksFolder;
    }

    public function setIsIncludeSource(bool $isIncludeSource): void
    {
        $this->isIncludeSource = $isIncludeSource;
    }

    public function setAssetsVersion(string $assetsVersion): void
    {
        $this->assetsVersion = $assetsVersion;
    }

    public function setResourceName(string $resourceName): void
    {
        $this->resourceName = $resourceName;
    }

    public function setJsExtension(string $jsExtension): void
    {
        $this->jsExtension = $jsExtension;
    }

    public function setCssExtension(string $cssExtension): void
    {
        $this->cssExtension = $cssExtension;
    }

    public function setPreviewExtension(string $previewExtension): void
    {
        $this->previewExtension = $previewExtension;
    }

    public function setCustomFrontFileName(string $customFrontFileName): void
    {
        $this->customFrontFileName = $customFrontFileName;
    }

    public function getPathToAssetsFolder(): string
    {
        return $this->pathToAssetsFolder;
    }

    public function getUrlOfAssetsFolder(): string
    {
        return $this->urlOfAssetsFolder;
    }

    public function getUrlOfBlocksFolder(): string
    {
        return $this->urlOfBlocksFolder;
    }

    public function isIncludeSource(): bool
    {
        return $this->isIncludeSource;
    }

    public function getAssetsVersion(): string
    {
        return $this->assetsVersion;
    }

    public function getResourceName(): string
    {
        return $this->resourceName;
    }

    public function getJsExtension(): string
    {
        return $this->jsExtension;
    }

    public function getCssExtension(): string
    {
        return $this->cssExtension;
    }

    public function getPreviewExtension(): string
    {
        return $this->previewExtension;
    }

    public function getCustomFrontFileName(): string
    {
        return $this->customFrontFileName;
    }

    public function getLateHeadFilter(): string
    {
        return $this->lateHeadFilter;
    }

    public function setLateHeadFilter(string $lateHeadFilter): void
    {
        $this->lateHeadFilter = $lateHeadFilter;
    }
}
