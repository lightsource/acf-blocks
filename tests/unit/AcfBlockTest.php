<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

use Codeception\Test\Unit;

class AcfBlockTest extends Unit
{
    public function testAddBlockClassReplacesCamelCaseToDash()
    {
        $acfBlock = new class extends AcfBlock {
            public function test()
            {
                $this->classes = [];
                $this->addBlockClass('SomeBlock');

                return $this->classes;
            }
        };

        $this->assertEquals([
                                'some-block'
                            ], $acfBlock->test());
    }

    public function testAddBlockClassAddsDoubleDashesToThemeWord()
    {
        $acfBlock = new class extends AcfBlock {
            public function test()
            {
                $this->classes = [];
                $this->addBlockClass('SomeBlockThemeRedOne');

                return $this->classes;
            }
        };

        $this->assertEquals([
                                'some-block--theme--red-one'
                            ], $acfBlock->test());
    }
}