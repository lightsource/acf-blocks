<?php

declare(strict_types=1);

namespace LightSource\AcfBlocks;

use Codeception\Test\Unit;
use Exception;
use LightSource\AcfBlocks\Interfaces\AcfDataInterface;
use Psr\Container\ContainerInterface;

class BlocksSetuperTest extends Unit
{
    public function testCallMethodWithDynamicArguments(): void
    {
        $dependency = new class {
            public int $data = 2;
        };
        $blocksSetuper = $this->make(BlocksSetuper::class, [
            'getMethodArgumentTypes' => function () use ($dependency) {
                return [get_class($dependency)];
            }
        ]);

        $object = new class {
            public int $data = 0;

            public function test($dependency): void
            {
                $this->data = $dependency->data;
            }
        };
        $container = $this->makeEmpty(ContainerInterface::class, [
            'get' => function ($name) use ($dependency) {
                return $name === get_class($dependency) ?
                    $dependency :
                    null;
            }
        ]);

        $blocksSetuper->callMethodWithDynamicArguments($object, 'test', $container);

        $this->assertEquals(2, $object->data);
    }

    public function testCallMethodWithDynamicArgumentsWhenExtraArgumentsAreSet(): void
    {
        $dependency = new class implements AcfDataInterface {
            private int $postId = 0;

            public function getPostId(): int
            {
                return $this->postId;
            }

            public function setPostId(int $postId): void
            {
                $this->postId = $postId;
            }
        };
        $blocksSetuper = $this->make(BlocksSetuper::class, [
            'getMethodArgumentTypes' => function () use ($dependency) {
                return [get_class($dependency)];
            }
        ]);

        $object = new class {
            public int $myPostId = 0;

            public function test(AcfDataInterface $data): void
            {
                $this->myPostId = $data->getPostId();
            }
        };
        $container = $this->makeEmpty(ContainerInterface::class, [
            'get' => function ($name) use ($dependency) {
                return $name === get_class($dependency) ?
                    $dependency :
                    null;
            }
        ]);

        $blocksSetuper->callMethodWithDynamicArguments($object, 'test', $container, [
            'postId' => 1721,
        ]);

        $this->assertEquals(1721, $object->myPostId);
    }

    public function testCallMethodWithDynamicArgumentsWhenArgumentsAreMissing(): void
    {
        $blocksSetuper = $this->make(BlocksSetuper::class);

        $object = new class {
            public int $data = 0;

            public function test(): void
            {
                $this->data = 1;
            }
        };
        $container = $this->makeEmpty(ContainerInterface::class, []);

        $blocksSetuper->callMethodWithDynamicArguments($object, 'test', $container);

        $this->assertEquals(1, $object->data);
    }

    public function testCallMethodWithDynamicArgumentsWhenMethodIsMissing(): void
    {
        $blocksSetuper = $this->make(BlocksSetuper::class, []);

        $object = new class {

        };
        $container = $this->makeEmpty(ContainerInterface::class, []);

        $blocksSetuper->callMethodWithDynamicArguments($object, 'test', $container);
    }

    public function testCallMethodWithDynamicArgumentsThrowsExceptionWhenDynamicArgumentIsNotClass(): void
    {
        $blocksSetuper = $this->make(BlocksSetuper::class, []);

        $object = new class {

            public function test(int $dependency): void
            {
            }
        };
        $container = $this->makeEmpty(ContainerInterface::class, []);
        $isWithException = false;

        try {
            $blocksSetuper->callMethodWithDynamicArguments($object, 'test', $container);
        } catch (Exception $exception) {
            $isWithException = true;
        }

        $this->assertTrue($isWithException);
    }
}
